# SESO

Proiektua konpilatzeko ondorengo ordena erabili (GCC behar da):

```bash
$ gcc -ansi src/* -o seso
```

Programa exekutatzeko:

```bash
$ ./seso
```
