#include "list.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0

void init_nodes(size_t len, struct node* nodes)
{
        size_t i;
        for (i = 0; i < len; ++i)
                nodes[i].free = TRUE;
}

int alloc_nodes(struct list* list)
{
        struct node *data = malloc(LIST_INITIAL_CAP * sizeof(struct node));
        if (data == NULL) {
                return -1;
        }

        init_nodes(LIST_INITIAL_CAP, data);
        list->data = data;

        list->last = NULL;
        list->first = NULL;

        list->capacity = LIST_INITIAL_CAP;
        list->size = 0;
        list->next_free = 0;

        return 0;
}

void free_nodes(struct list* list)
{
        free(list->data);
}

/* **MAKES A COPY** of the given struct and inserts it in the list
 * if there is no free space the item wont be inserted and
 * -1 will be returned.
 */
int insert_item(struct list *list, const T *item)
{
        struct node *n;
        size_t i;

        assert(item != NULL && "The given node is NULL");
        assert(list != NULL && "The given list is NULL");

        /* The list is full, return with error */
        if (list->size >= list->capacity) {
                return -1;
        }

        n = &list->data[list->next_free];

        /* If the list is empty set this node as the first */
        if (list->size == 0) {
                list->first = n;
        } else {
                list->last->next = n;
        }

        n->next = NULL;
        n->previous = list->last;
        n->free = FALSE;

        memcpy(&n->item, item, sizeof(T));

        list->last = n;

        /* Find fordward the next free node */
        for (i = list->next_free; i < list->capacity; ++i) {
                if (list->data[i].free) {
                        break;
                }
        }
        list->next_free = i;
        list->size += 1;

        return 0;
}

void remove_item(struct list *list, struct node *node)
{
        size_t i;
        assert(node != NULL && "The given node is NULL");
        assert(list != NULL && "The given list is NULL");
        assert(list->size != 0 && "The given list is empty");

        if (node->previous != NULL) {
                node->previous->next = node->next;
        }
        if (node->next != NULL) {
                node->next->previous = node->previous;
        }
        if (list->first == node) {
                list->first = node->next;
        }
        if (list->last == node) {
                list->last = node->previous;
        }
        node->next = NULL;
        node->previous = NULL;
        node->free = TRUE;

        if ((list->data + list->next_free) > node) {
                list->next_free = node - list->data;
        }
        list->size -= 1;
}

struct node *get_item(struct list *list, unsigned pid)
{
        struct node *i = list->first;
        while (i != NULL && i->item.pid != pid) {
                i = i->next;
        }

        return i;
}


