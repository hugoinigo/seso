#ifndef CPU_H_
#define CPU_H_
#include "definitions.h"

#define NUM_CORES 1
#define NUM_REGISTERS 16

/* struct mmu { */
/*         unsigned tlb : ADDR; */
/* }; */

struct core {
        unsigned pc   : ADDR_BUS_BITS;
        unsigned ir   : DATA_BUS_BITS;
        unsigned ptbr : ADDR_BUS_BITS;
        unsigned r[NUM_REGISTERS];
        /* struct mmu mmu; */
};

struct cpu {
        unsigned int num_cores;

        struct core cores[NUM_CORES];
};

extern struct cpu CPU;
void exec_proc(struct PCB *pcb);

#endif /* CPU_H_ */
