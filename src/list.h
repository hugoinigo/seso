#ifndef LIST_H_
#define LIST_H_

#include <stdlib.h>
#include "definitions.h"

#define LIST_INITIAL_CAP MAX_PROC_CAP

struct node {
        T item;
        struct node *next;
        struct node *previous;
        int free;
};

struct list {
        struct node *data;
        struct node *first;
        struct node *last;
        size_t capacity;
        size_t size;
        size_t next_free;
};

int alloc_nodes(struct list* list);
void free_nodes(struct list* list);
int insert_item(struct list* list, const T *item);
void remove_item(struct list *list, struct node *node);
struct node *get_item(struct list *list, unsigned pid);

#endif /* LIST_H_ */
