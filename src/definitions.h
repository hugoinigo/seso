#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

#define MAX_PROC_CAP 100
#define NUM_PROGRAMS 50

/* default PCB parameters */
#define INITIAL_QUANTUM 20
#define DEF_NICE 0
#define TL 20

/* memory definitions */
#define ADDR_BUS_BITS 24
#define DATA_BUS_BITS 32

struct memory_management {
        unsigned pgb  : ADDR_BUS_BITS;
        unsigned code : ADDR_BUS_BITS;
        unsigned data : ADDR_BUS_BITS;
};

enum proc_status {
        READY,
        RUNNING,
        DONE
};

struct PCB {
        unsigned pid;
        unsigned quantum;
        int time;
        int nice;

        unsigned pc;
        enum proc_status status;

        /* hau soilik erabiliko dugu memoria
           irteera estandarrean erakusteko */
        unsigned len;

        struct memory_management mm;
};

#define T struct PCB

void print_PCB(const struct PCB *PCB);
unsigned min(unsigned a, unsigned b);

#endif /* DEFINITIONS_H_ */
