#ifndef MEMORY_H_
#define MEMORY_H_
#include "definitions.h"

#define NO_MEM 0x01FFFFFF

#define VM_MIN 0x400000
#define VM_MAX 0xFFFFFF

#define PM_MIN 0x000000
#define PM_MAX 0xFFFFFF

#define KERNEL_MIN 0x000000
#define TABLE_MIN 0x000400
#define TABLE_MAX 0x3FFFFF

#define PAGE_BITS 8

/* ----- */
#define TABLE_BITS (ADDR_BUS_BITS - PAGE_BITS)
#define TABLE_SIZE (1 << TABLE_BITS)
#define PAGE_SIZE (1 << PAGE_BITS)

/* #define PAGE_SIZE 0x100 /\* 256B 2^8 */
/* #define PAGE_TABLE_SIZE 0x10000 /\* 64KB 2^16 *\/ */

extern unsigned *mem;

unsigned *init_memory();

unsigned alloc_table(unsigned pid);
void free_table(unsigned pid);

unsigned alloc_page(unsigned pid);
void free_pages(unsigned pid);

unsigned find_context_addr(unsigned pid);

#endif /* MEMORY_H_ */
