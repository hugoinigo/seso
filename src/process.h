#ifndef PROCESS_H_
#define PROCESS_H_

#define MAX_PROG_LEN 1024

void *process_generator(void *_);
void *loader(void *_);

void load(unsigned id);
void print_load_res();

void end_process(unsigned pid);

#endif /* PROCESS_H_ */
