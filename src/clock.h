#ifndef CLOCK_H_
#define CLOCK_H_
#include <semaphore.h>

extern int num_timers;
extern sem_t proc_sem, proc_sem2;
extern sem_t sched_sem, sched_sem2;

void init_clock();
void *run_clock(void *usec);
void *run_sched_timer(void *p);
void *run_proc_timer(void *p);

#endif /* CLOCK_H_ */
