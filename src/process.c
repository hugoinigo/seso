#include <stddef.h>
#include <stdlib.h>
#include <semaphore.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "clock.h"
#include "definitions.h"
#include "list.h"
#include "scheduler.h"
#include "memory.h"
#include "process.h"

#define OK 0

static struct list proc_list;
static unsigned int pid_count = 1;

sem_t proc_sem, proc_sem2;

static struct {
        unsigned text : ADDR_BUS_BITS;
        unsigned data : ADDR_BUS_BITS;
        unsigned len;
        unsigned words[MAX_PROG_LEN];
} load_res;

int fill_pcb(struct PCB *pcb, unsigned int pid)
{
        unsigned res;

        pcb->pid = pid;
        pcb->time = load_res.data >> 2;
        pcb->quantum = INITIAL_QUANTUM;
        pcb->nice = (rand() % 20) - 10;

        res = alloc_table(pid);
        if (res == NO_MEM) {
                return -1;
        }

        pcb->len = load_res.len;

        pcb->mm.pgb = res;
        pcb->mm.code = load_res.text;
        pcb->mm.data = load_res.data;

        pcb->pc = pcb->mm.code;
        pcb->status = READY;

        return OK;
}

void dump_data_to_mem(const struct PCB *pcb)
{
        unsigned i, j;
        unsigned waddr, paddr;

        unsigned num_pages = load_res.len / PAGE_SIZE + 1;

        unsigned min_len;
        unsigned len = load_res.len;
        unsigned pgb = pcb->mm.pgb;

        printf("num pages: %d\n", num_pages);

        printf("table:\n");
        /* for (i = 0; i < num_pages; ++i) { */
        /*         printf("%06X    %08X\n", pgb + i, mem[pgb + i]); */
        /* } */

        for (i = 0; i < num_pages; ++i) {
                paddr = alloc_page(pcb->pid);
                if (paddr == NO_MEM) {
                        printf("alloc_page: ENOMEM\n");
                        return;
                }

                mem[pgb + i] = paddr;
                printf("%06X    %08X\n", pgb + i, mem[pgb + i]);

                min_len = min(len, PAGE_SIZE);
                for (j = 0; j < min_len; ++j) {
                        waddr = paddr + j;
                        mem[waddr] = load_res.words[i * PAGE_SIZE + j];
                        /* printf("%06X %06x %08X\n", waddr, j*4, mem[waddr]); */
                }

                len -= min_len;
        }
}

void *process_generator(void *_)
{
        struct PCB pcb;
        srand(0);
        alloc_nodes(&proc_list);

        while (1) {
                sem_wait(&proc_sem);

                if (proc_list.size < proc_list.capacity) {
                        printf("enqueue: \t");
                        fill_pcb(&pcb, pid_count++);
                        print_PCB(&pcb);

                        insert_item(&proc_list, &pcb);
                        enqueue_element(&proc_list.last->item);
                /* } else { */
                /*         printf("-- pcb list is full\n"); */
                }
        }

        return NULL;
}

void *loader(void *_)
{
        struct PCB pcb;
        unsigned pid = 0;
        int res;

        alloc_nodes(&proc_list);
        srand(0);

        while (1) {
                sem_wait(&proc_sem);
                /* sleep(1); */

                if (pid >= NUM_PROGRAMS) {
                /* if (pid >= 2) { */
                        printf("ALL PROGRAMS LOADED\n");
                        sem_post(&proc_sem2);
                        continue;
                }
                load(pid); /* ez da optimoena */
                res = fill_pcb(&pcb, pid+1);
                if (res == OK) {
                        print_PCB(&pcb);
                        insert_item(&proc_list, &pcb);
                        enqueue_element(&proc_list.last->item);
                        dump_data_to_mem(&pcb);

                        ++pid;
                } else {
                        printf("ENOMEM\n");
                }
                sem_post(&proc_sem2);
        }
}

void end_process(unsigned pid)
{
        struct node *n = get_item(&proc_list, pid);
        if (n != NULL) {
                free_table(pid);
                free_pages(pid);
                remove_item(&proc_list, n);
        }
}

void load(unsigned id)
{
        char cname[12];
        FILE *fp;

        unsigned text, data, word = 0;
        unsigned len = 0;
        int result;

        sprintf(cname, "prog%03d.elf", id);
        fp = fopen(cname, "r");

        /* erabiliko dugu fscanf */
        if (fscanf(fp, ".text %X\n", &text) != 1) {
                printf("error .text\n");
        }

        if (fscanf(fp, ".data %X\n", &data) != 1) {
                printf("error .data\n");
        }

        while (fscanf(fp, "%X\n", &word) == 1) {
                load_res.words[len++] = word;
        }

        load_res.text = text;
        load_res.data = data;
        load_res.len = len;

        fclose(fp);
}

void print_load_res()
{
        unsigned i;

        printf("text -> 0x%X\n", load_res.text);
        printf("data -> 0x%X\n", load_res.data);
        printf("len: %d\n", load_res.len);

        for (i = 0; i < load_res.len; ++i) {
                printf("0x%06X  0x%08X\n", i*4, load_res.words[i]);
        }
}
