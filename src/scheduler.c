/* #include <stdio.h> */
#include <malloc.h>
#include <semaphore.h>
#include <pthread.h>

#include "scheduler.h"
#include "definitions.h"
#include "process.h"
#include "cpu.h"

#define NICE_OFFS 10
#define DEF_QUANTUM 3

sem_t sched_sem, sched_sem2;
pthread_mutex_t pq_mutex;

struct process_queue {
        struct PCB **data;
        size_t size;
        size_t front, back;
} pq;

static const int weight_table[20] = {
        9548, 7620, 6100, 4904, 3906,
        3121, 2501, 1991, 1586, 1277,
        1024, 820, 655, 526, 423,
        335, 272, 215, 172, 137,
};

int enqueue_element(const struct PCB *pcb)
{
        pthread_mutex_lock(&pq_mutex);

        if (pq.size == MAX_PROC_CAP) {
                /* the memory is full */
                pthread_mutex_unlock(&pq_mutex);
                return -1;
        }

        pq.back = (pq.front + pq.size) % MAX_PROC_CAP;
        pq.data[pq.back] = (struct PCB*) pcb;
        pq.size++;

        pthread_mutex_unlock(&pq_mutex);
        return 0;
}

int dequeue_element(struct PCB **pcb)
{
        pthread_mutex_lock(&pq_mutex);

        if (pq.size == 0) {
                /* there are no elements in the queue */
                pthread_mutex_unlock(&pq_mutex);
                return -1;
        }

        *pcb = pq.data[pq.front];
        pq.front = (pq.front + 1) % MAX_PROC_CAP;
        pq.size--;

        pthread_mutex_unlock(&pq_mutex);
        return 0;
}

void init_queue()
{
        pthread_mutex_init(&pq_mutex, NULL);

        pq.data = malloc(sizeof(struct PCB **) * MAX_PROC_CAP);
        pq.size = 0;
        pq.front = 0;
        pq.back = 0;
}

void fcfs()
{
        struct PCB *pcb;
        if (dequeue_element(&pcb) != 0) {
                printf("-- no processes left in queue\n");
        } else {
                printf("dispatched: \t");
                print_PCB(pcb);
        }
}

void exec_process(struct PCB *pcb)
{
        unsigned int i;
        for (i = 0; i < pcb->quantum; ++i) {
                if (pcb->time <= 0) {
                        return;
                }
                pcb->time -= 1;
        }
}

void recalc_quantum(struct PCB *pcb)
{
        unsigned weight_sum = 0;
        unsigned i, start, end;
        unsigned timeslice;
        unsigned nice;

        if (pq.front < pq.back) {
                start = pq.front;
                end = pq.back;
        } else {
                start = pq.back;
                end = pq.front;
        }

        if (pq.size == 0) {
                weight_sum += weight_table[pcb->nice + NICE_OFFS];
        } else {
                for (i = start; i <= end; ++i) {
                        nice = pq.data[i]->nice + NICE_OFFS; 
                        weight_sum += weight_table[nice];
                }
        }

        timeslice = TL * (float) (1024.0 / (float) weight_sum);
        if (timeslice == 0) {
                timeslice = DEF_QUANTUM;
        }
        pcb->quantum = timeslice;
}

void rr()
{
        struct PCB *pcb;
        if (dequeue_element(&pcb) != 0) {
                printf("-- no processes left in queue\n");

        } else {
                /* pcb->time -= pcb->quantum; */
                recalc_quantum(pcb);
                exec_proc(pcb);

                if (pcb->time > 0) {
                        enqueue_element(pcb);
                        print_PCB(pcb);
                } else {
                        printf("dispatched: \t");
                        print_PCB(pcb);
                        end_process(pcb->pid);
                }
        }
}

void *run_scheduler(void *_)
{
        init_queue();

        while (1) {
                sem_wait(&sched_sem);
                rr();
                sem_post(&sched_sem2);
        }
        return NULL;
}

