#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "process.h"
#include "list.h"
#include "clock.h"
#include "scheduler.h"
#include "memory.h"
#include "cpu.h"

int num_timers;

void probatu_lista()
{
        struct list l;
        struct PCB pcb;
        size_t i;

        int result = alloc_nodes(&l);
        insert_item(&l, &pcb);
        insert_item(&l, &pcb);
        insert_item(&l, &pcb);
        insert_item(&l, &pcb);
        insert_item(&l, &pcb);
        remove_item(&l, &l.data[3]);
        remove_item(&l, l.first);
        insert_item(&l, &pcb);
        /* insert_item(&l, &pcb); */

        for (i = 0; i < 10; ++i) {
                struct node n = l.data[i];
                size_t next = !n.next ? 0 : n.next - l.data;
                size_t prev = !n.previous ? 0 : n.previous - l.data;
                printf("%lu\t n: %lu p:%lu %s\n", i, next, prev,
                       n.free ? "FREE":"OCCUP");
        }
        printf("first: %lu\nlast: %lu\n", l.first-l.data, l.last-l.data);
        free_nodes(&l);
}

int main(int argc, char *argv[])
{
        pthread_t clock;
        pthread_t proc_timer, sched_timer;

        pthread_t proc_gen, scheduler;

        unsigned proc_T = 7;
        unsigned sched_T = 3;
        unsigned clock_freq = 50000;

        /* load(0); */
        /* print_load_res(); */
        /* return 0; */

        num_timers = 2;
        mem = init_memory();
        init_clock();

        pthread_create(&clock, NULL, run_clock, &clock_freq);
        pthread_create(&sched_timer, NULL, run_sched_timer, &sched_T);
        pthread_create(&proc_timer, NULL, run_proc_timer, &proc_T);
        /* pthread_create(&proc_gen, NULL, process_generator, NULL); */
        pthread_create(&proc_gen, NULL, loader, NULL);
        pthread_create(&scheduler, NULL, run_scheduler, NULL);

        pthread_join(clock, NULL);
        pthread_join(proc_timer, NULL);
        pthread_join(sched_timer, NULL);
        pthread_join(proc_gen, NULL);
        pthread_join(scheduler, NULL);
        return 0;
}
