#include <stdio.h>
#include "definitions.h"

void print_PCB(const struct PCB *PCB)
{
        printf("PCB (%p) -> pid (%d)\n", PCB, PCB->pid);
        printf("           time (%d)\n", PCB->time);
        printf("           nice (%d)\n", PCB->nice);
        printf("        quantum (%d)\n", PCB->quantum);
        printf("     page table (0x%x)\n", PCB->mm.pgb);
}

unsigned min(unsigned a, unsigned b)
{
        if (a < b) {
                return a;
        } else {
                return b;
        }
}
