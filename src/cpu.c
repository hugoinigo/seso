#include "definitions.h"
#include "cpu.h"
#include "memory.h"

#include <stdio.h>

#define OFFS_MASK 0x000000FF
#define PAGE_MASK 0x00FFFF00

#define INST_MASK 0xF0000000
#define REG_MASK  0x0F000000
#define ADDR_MASK 0x00FFFFFF
#define R1_MASK   0x00F00000
#define R2_MASK   0x000F0000

#define NEXT_PC 4

#define INST_LD   0x0
#define INST_ST   0x1
#define INST_ADD  0x2
#define INST_EXIT 0xF

#define EXIT 1
#define OK 0

struct cpu CPU = {
        NUM_CORES,
        {0},
};

unsigned translate_va_to_pa(unsigned table_addr,
                        unsigned vm_addr)
{
        unsigned offs, page;
        unsigned page_head;

        vm_addr >>= 2;

        offs = vm_addr & OFFS_MASK;
        page = vm_addr & PAGE_MASK;
        page_head = mem[table_addr+page];

        return page_head + offs;
}

void load_ir(struct core *c) {
        unsigned paddr;
        paddr = translate_va_to_pa(c->ptbr, c->pc);
        c->ir = mem[paddr];
}

void load_context(struct core *c, const struct PCB *pcb)
{
        unsigned i;
        unsigned addr = find_context_addr(pcb->pid);
        if (addr == NO_MEM) {
                return;
        }

        for (i = 0; i < NUM_REGISTERS; ++i) {
                c->r[i] = mem[addr + i];
        }
}

void store_context(struct core *c, const struct PCB *pcb)
{
        unsigned i;
        unsigned addr = find_context_addr(pcb->pid);
        if (addr == NO_MEM) {
                return;
        }

        for (i = 0; i < NUM_REGISTERS; ++i) {
                mem[addr + i] = c->r[i];
        }
}

void print_core(const struct core *c)
{
        printf("c -> pc   0x%06X\n", c->pc);
        printf("     ir 0x%08X\n",   c->ir);
        printf("     ptbr 0x%06X\n", c->ptbr);
}

int exec_inst(struct core *core)
{
        int res = OK;
        unsigned ir = core->ir;
        unsigned c = ir & INST_MASK;
        unsigned r = ir & REG_MASK;
        unsigned vaddr, paddr, word;
        unsigned r1, r2;

        c >>= 28;
        r >>= 24;

        switch (c) {
        case INST_LD:
                vaddr = ir & ADDR_MASK;
                paddr = translate_va_to_pa(core->ptbr, vaddr);

                core->r[r] = mem[paddr];

                break;
        case INST_ST:
                vaddr = ir & ADDR_MASK;
                paddr = translate_va_to_pa(core->ptbr, vaddr);

                mem[paddr] = core->r[r];

                break;
        case INST_ADD:
                r1 = ir & R1_MASK;
                r2 = ir & R2_MASK;

                r1 >>= 20;
                r2 >>= 16;

                core->r[r] = core->r[r1] + core->r[r2];

                break;
        case INST_EXIT:

                res = EXIT;
                break;
        }

        return res;
}

void fprintm(FILE *fp, const struct PCB *pcb)
{
        unsigned min_len, len = pcb->len;
        unsigned num_pages = len / PAGE_SIZE + 1;
        unsigned i, j;
        unsigned waddr, paddr;

        fprintf(fp, ".text %06X\n", pcb->mm.code);
        fprintf(fp, ".data %06X\n", pcb->mm.data);

        for (i = 0; i < num_pages; ++i) {
                paddr = mem[pcb->mm.pgb + i];
                min_len = min(len, PAGE_SIZE);
                for (j = 0; j < min_len; ++j) {
                        waddr = paddr + j;
                        /* fprintf(fp, "0x%06X: %08X\n", (i*PAGE_SIZE+j)*4, mem[waddr]); */
                        fprintf(fp, "%08X\n", mem[waddr]);
                }

                len -= min_len;
        }
}

void store_results(const struct PCB *pcb)
{
        FILE *fp;
        char cname[11];
        sprintf(cname, "pid%03d.txt", pcb->pid-1);

        fp = fopen(cname, "w+");

        fprintm(fp, pcb);

        fclose(fp);
}

void exec_proc(struct PCB *pcb)
{
        struct core *c = &CPU.cores[0];
        unsigned quantum = min(pcb->time, pcb->quantum);
        int res;
        unsigned i;

        c->pc = pcb->pc;
        c->ptbr = pcb->mm.pgb;
        pcb->status = RUNNING;

        load_context(c, pcb);

        printf("pid (%d)\n", pcb->pid);
        for (i = 0; i < quantum; ++i) {
                load_ir(c);
                print_core(c);

                res = exec_inst(c);
                if (res == EXIT) {
                        store_results(pcb);

                        pcb->status = DONE;
                        pcb->time -= quantum;
                        pcb->pc = c->pc;

                        return;
                }

                c->pc += NEXT_PC;
        }
        store_context(c, pcb);

        pcb->status = READY;
        pcb->time -= quantum;
        pcb->pc = c->pc;
}
