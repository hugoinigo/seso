#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <semaphore.h>
#include "clock.h"
#include "process.h"

pthread_mutex_t mutex;

int done = 0;
pthread_cond_t cond1, cond2;

void init_clock()
{
        pthread_mutex_init(&mutex, NULL);
        sem_init(&proc_sem, 0, 0);
        sem_init(&proc_sem2, 0, 0);
        sem_init(&sched_sem, 0, 0);
        sem_init(&sched_sem2, 0, 0);

        pthread_cond_init(&cond1, NULL);
        pthread_cond_init(&cond2, NULL);
}

void destroy_clock()
{
        pthread_mutex_destroy(&mutex);
        sem_destroy(&proc_sem);
        sem_destroy(&proc_sem2);
        sem_destroy(&sched_sem);
        sem_destroy(&sched_sem2);

        pthread_cond_destroy(&cond1);
        pthread_cond_destroy(&cond2);
}

void *run_clock(void *usec)
{
        unsigned useconds = *(unsigned *) usec;

        while (1) {
                pthread_mutex_lock(&mutex);
                while (done < num_timers) {
                        pthread_cond_wait(&cond1, &mutex);
                }
                /*-----*/
                usleep(useconds);
                /* printf("[C]\n"); */

                /*-----*/
                done = 0;
                pthread_cond_broadcast(&cond2);
                pthread_mutex_unlock(&mutex);
        }
}

void *run_sched_timer(void *p)
{
        const unsigned period = *(unsigned *) p;
        unsigned int counter = 0;
        pthread_mutex_lock(&mutex);
        while (1) {
                done++;
                /*-----*/
                counter++;

                if (counter == period) {
                        counter = 0;
                        printf("[S]\n");
                        sem_post(&sched_sem);
                        sem_wait(&sched_sem2);
                }

                /*-----*/
                pthread_cond_signal(&cond1);
                pthread_cond_wait(&cond2, &mutex);
        }
}

void *run_proc_timer(void *p)
{
        const unsigned period = *(unsigned *) p;
        unsigned int counter = 0;
        pthread_mutex_lock(&mutex);
        while (1) {
                done++;
                /*-----*/
                counter++;
                /* printf("counter: %d\n", counter); */

                if (counter == period) {
                        counter = 0;
                        printf("[P]\n");
                        sem_post(&proc_sem);
                        sem_wait(&proc_sem2);
                }
                /*-----*/
                pthread_cond_signal(&cond1);
                pthread_cond_wait(&cond2, &mutex);
        }
}

