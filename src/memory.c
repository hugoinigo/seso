#include "memory.h"
#include "cpu.h"
#include <malloc.h>

#define TABLE_MANAGER_SIZE (TABLE_MAX - TABLE_MIN) / TABLE_SIZE
#define PAGE_MANAGER_SIZE (VM_MAX - VM_MIN) / PAGE_SIZE
#define CONTEXT_MANAGER_SIZE (KERNEL_MIN + TABLE_MIN) / NUM_REGISTERS

unsigned *mem;

static unsigned table_manager[(unsigned) TABLE_MANAGER_SIZE];
static unsigned page_manager[(unsigned) PAGE_MANAGER_SIZE];
static unsigned context_manager[(unsigned) PAGE_MANAGER_SIZE];

unsigned *init_memory()
{
        unsigned i;
        printf("num tables: %d\n", TABLE_MANAGER_SIZE);
        printf("num pages: %d\n", PAGE_MANAGER_SIZE);
        printf("num cont: %d\n", CONTEXT_MANAGER_SIZE);
        for (i = 0; i < TABLE_MANAGER_SIZE; ++i) {
                table_manager[i] = 0;
                page_manager[i] = 0;
        }
        return calloc(PM_MAX + 1, sizeof(unsigned));
}

unsigned alloc_table(unsigned pid)
{
        unsigned i;
        for (i = 0; i < TABLE_MANAGER_SIZE; ++i) {
                if (table_manager[i] == 0) {
                        table_manager[i] = pid;
                        context_manager[i] = pid;

                        return TABLE_MIN + i * TABLE_SIZE;
                }
        }

        return NO_MEM;
}

unsigned alloc_page(unsigned pid)
{
        unsigned i;
        for (i = 0; i < PAGE_MANAGER_SIZE; ++i) {
                if (page_manager[i] == 0) {
                        page_manager[i] = pid;
                        return VM_MIN + i * PAGE_SIZE;
                }
        }

        return NO_MEM;
}

void free_table(unsigned pid)
{
        unsigned i;
        for (i = 0; i < TABLE_MANAGER_SIZE; ++i) {
                if (table_manager[i] == pid) {
                        table_manager[i] = 0;
                        context_manager[i] = 0;

                        return;
                }
        }
}

void free_pages(unsigned pid)
{
        unsigned i;
        for (i = 0; i < PAGE_MANAGER_SIZE; ++i) {
                if (page_manager[i] == pid) {
                        page_manager[i] = 0;

                        /* return; */
                }
        }
}

unsigned find_context_addr(unsigned pid)
{
        unsigned i;
        for (i = 0; i < CONTEXT_MANAGER_SIZE; ++i) {
                if (context_manager[i] == pid) {
                        return KERNEL_MIN + i * NUM_REGISTERS;
                }
        }

        return NO_MEM;
}

