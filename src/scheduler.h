#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "definitions.h"

/* extern pthread_mutex_t pq_mutex; */

int enqueue_element(const struct PCB *pcb);
int dequeue_element(struct PCB **pcb);

void *run_scheduler(void *_);

#endif /* SCHEDULER_H_ */
