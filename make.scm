#!/usr/bin/env guile
!#

(use-modules (eraiki))

(define cc "gcc")
(define cflags '("-std=c89" "-O3" "-Wpedantic" "-Werror"))

(define target "seso")
(define src-dir (path "src"))
(define obj-dir (path "obj"))

(define src-files (list-path src-dir ".c"))
(define o-files (prefix-path obj-dir (switch-extension src-files ".c" ".o")))

(define c-files (prefix-path src-dir src-files))
(define h-files (prefix-path src-dir (list-path src-dir ".h")))

(mkpath obj-dir)

(for-cons (o-file . c-file) in (zip o-files c-files)
          (cmd cc "-c" c-file "-o" o-file cflags))


(cmd cc "-o" target o-files cflags)
  
